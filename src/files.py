import shutil
import os
import logging
import progressbar
from models.exif_data import ExifData

class Files:
  """This Class is for handling files"""
  def __init__(self, images:list, dst_folder:str, logger:logging) -> None:
    self.__images = images
    self.__dst_folder = dst_folder
    self.__logger = logger
    self.__image_total = len(self.__images)

  def __move_image(self, image:ExifData):
    src = f"{image.path}/{image.name}"
    dst = f"{self.__dst_folder}/{image.make}/{image.year}/{image.month}/{image.day}"
    if not os.path.exists(path=dst):
      os.makedirs(name=dst)
    image_info = os.stat(image.path)
    
    try:
      shutil.move(src=src, dst=dst)
      os.chmod(path=f"{dst}/{image.name}", mode=image_info.st_mode)
      self.__logger.info(f"Moved {src} -> {dst}")
    except Exception as err:
      self.__logger.error(err)

  def sort_pictures(self):
    progress_bar = progressbar.progressbar.ProgressBar()
    progress_bar.maxval =  self.__image_total
    progress_bar.term_width = 40

    for image_counter, image in enumerate(self.__images):
      image:ExifData
      self.__move_image(image=image)
      progress_bar.update(image_counter)

    progress_bar.finish()
    print(f"Finished. { self.__image_total} images sorted in {progress_bar.seconds_elapsed} seconds.")
