import pathlib
import logging
import os

class scanDir:
  """Scan directories"""
  def __init__(self, logger:logging) -> None:
    self.__list_for_recursive_folder_scan = []
    self.__logger = logger

  def scanDir(self, path:str):
    pass

  def recursiveScanDir(self, path_for_scan:str):
    """This methode will scan the given folder recursive"""
    self.__logger.info("Start folder Scan")
    results = sorted(pathlib.Path(f"{path_for_scan}").glob("**/*"))

    for data in results:
      if os.path.isfile(pathlib.Path(data)):
        self.__list_for_recursive_folder_scan.append(data)

    return self.__list_for_recursive_folder_scan

  def countImages(self):
    return len(self.__list_for_recursive_folder_scan)