import yaml

class Config:
  def __init__(self) -> None:
    config = self.__load()
    self.src = config['source']
    self.dst = config['destination']

  def __load(self):
    with open('config.yml', 'r') as file:
      return yaml.safe_load(file)
    
