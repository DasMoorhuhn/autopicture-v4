class ExifData:
  """This is for an object that stores the data of a picture"""
  def __init__(self, imagePath, imageName, day, month, year, time, make) -> None:
    self.path = str(imagePath)
    self.name = str(imageName)
    self.day = int(day)
    self.month = int(month)
    self.year = int(year)
    self.time = str(time)
    self.make = str(make)