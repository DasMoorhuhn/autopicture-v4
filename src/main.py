import logging

from files import Files
from scan_directory import scanDir
from config import Config

logger = logging.getLogger('autoPicture')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='autoPicture.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('%(asctime)s|%(levelname)s|%(name)s|:%(message)s'))
logger.addHandler(handler)

config = Config()
scan_dir = scanDir(logger=logger)


images = scan_dir.recursiveScanDir(path_for_scan=config.src)
for image in images:
    print(image)

file_handler = Files(images=image, logger=logger, dst_folder=config.dst)
file_handler.sort_pictures()
